﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Infra.Data.EntityConfig;
using TradeManager.Domain.Entities;
using System.Data.Entity.Infrastructure;

namespace TradeManager.Infra.Data.Contexto
{
    public class TradeManagerContext : DbContext
    {
        public DbSet<Exchange> Exchanges { get; set; }
        public DbSet<Trade> Trades { get; set; }
        public DbSet<ParametrosAplicacao> ParametrosAplicacao { get; set; }

        public TradeManagerContext()
            : base("TradeManagerConnection")
        {
            Configuration.LazyLoadingEnabled = true;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Properties()
                .Where(p => p.Name == p.ReflectedType.Name + "Id")
                .Configure(p => p.IsKey());

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            modelBuilder.Configurations.Add(new ExchangeConfiguration());
            modelBuilder.Configurations.Add(new TradeConfiguration());
            modelBuilder.Configurations.Add(new ParametrosAplicacaoConfiguration());
            modelBuilder.Configurations.Add(new UsuarioConfiguration());
        }
    }
}
