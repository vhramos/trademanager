﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Repositories;

namespace TradeManager.Infra.Data.Repositories
{
    public class ExchangeRepository : RepositoryBase<Exchange>, IExchangeRepository
    {
    }
}
