﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;

namespace TradeManager.Infra.Data.EntityConfig
{
    public class ParametrosAplicacaoConfiguration : EntityTypeConfiguration<ParametrosAplicacao>
    {
        public ParametrosAplicacaoConfiguration()
        {
            HasKey(c => c.ParametrosAplicacaoId);
        }

    }
}
