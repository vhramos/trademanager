﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Infra.Data.ContextoConfig
{
    public class TradeManagerContextConfig : DbConfiguration
    {
        private const string SQL_CLIENT_PROVIDER = "System.Data.SqlClient";
        public TradeManagerContextConfig()
        {
            SetExecutionStrategy(SQL_CLIENT_PROVIDER, () => new TradeManagerExecutionStrategy());
        }
    }
}
