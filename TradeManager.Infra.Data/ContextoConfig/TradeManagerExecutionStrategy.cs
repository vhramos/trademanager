﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Infra.Data.ContextoConfig
{
    public class TradeManagerExecutionStrategy : DbExecutionStrategy
    {
        public TradeManagerExecutionStrategy()
        {
        }
        public TradeManagerExecutionStrategy(int maxRetryCount, TimeSpan maxDelay)
            : base(maxRetryCount, maxDelay)
        {
        }
        protected override bool ShouldRetryOn(Exception exception)
        {
            return exception is SqlException || exception is Win32Exception;
        }
    }
}
