﻿using Ninject;
using Ninject.Parameters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradeManager.Application.AppService;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Interface.Task;
using TradeManager.CrossCutting.IoC;
using TradeManager.Presentation.Forms;

namespace TradeManager.Presentation
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            var kernel = new StandardKernel();
            kernel.Bind<FrmPrincipal>().To<FrmPrincipal>();
            kernel.Bind<FrmLogin>().To<FrmLogin>();
            BootStrapper.RegisterServices(kernel);
            System.Windows.Forms.Application.Run(kernel.Get<FrmLogin>(new ConstructorArgument("kernel", kernel)));
        }
    }
}
