﻿using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradeManager.Application.Interface.AppService;
using TradeManager.CrossCutting.Crypto;
using TradeManager.Domain.Entities;
using TradeManager.Presentation.Util;

namespace TradeManager.Presentation.Forms
{
    public partial class FrmLogin : Form
    {
        private Usuario _usuarioLogado;
        private readonly IUsuarioAppService _usuarioAppService;
        private StandardKernel _kernel;
        private bool _primeiroAcesso;
        public FrmLogin(StandardKernel kernel, IUsuarioAppService usuarioAppService)
        {
            _usuarioAppService = usuarioAppService;
            _kernel = kernel;
            InitializeComponent();
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            ExibirLoader();
            bgwLogin.RunWorkerAsync();
        }
        private void ExibirLoader()
        {
            Invoke(new EventHandler(delegate
            {
                pbLoader.Visible = true;
                gpLogin.Visible = false;
                gpPrimeiroAcesso.Visible = false;
                btnLogin.Visible = false;
            }));
        }
        private void ExibirFormulario()
        {
            Invoke(new EventHandler(delegate
            {
                pbLoader.Visible = false;
                gpLogin.Visible = !_primeiroAcesso;
                gpPrimeiroAcesso.Visible = _primeiroAcesso;
                btnLogin.Visible = true;
            }));
        }
        private void EfetuarLogin(Usuario usuario)
        {
            if (IsHandleCreated)
            {
                Invoke(new EventHandler(delegate
                {
                    Hide();
                    Ambiente.UsuarioLogado = usuario;
                    _kernel.Get<FrmPrincipal>().ShowDialog();
                }));
            }
        }
        private void bgwLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!_primeiroAcesso)
            {
                Usuario usuario = _usuarioAppService.EfetuarLogin(
                    new Usuario()
                    {
                        Login = txtLogin.Text,
                        Senha = txtSenha.Text
                    });

                if (usuario != null)
                {
                    if (usuario.PrimeiroAcesso)
                    {
                        _primeiroAcesso = usuario.PrimeiroAcesso;
                        _usuarioLogado = usuario;
                        ExibirFormulario();
                    }
                    else
                        EfetuarLogin(usuario);
                }
                else
                {
                    ExibirFormulario();
                    MessageBox.Show("Usuário e/ou senha inválidos!", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                if (CryptoUtil.HashPassword(txtSenhaAtual.Text) == _usuarioLogado.Senha)
                {
                    _usuarioLogado.PrimeiroAcesso = false;
                    _usuarioLogado.Senha = txtSenhaNova.Text;
                    _usuarioAppService.Update(_usuarioLogado);
                    MessageBox.Show("Senha renovada!", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    EfetuarLogin(_usuarioLogado);
                }
                else
                {
                    ExibirFormulario();
                    MessageBox.Show("Senha atual inválida!", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}
