﻿namespace TradeManager.Presentation.Forms
{
    partial class FrmExchange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExchange));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSigla = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTaxaSaque = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTaxaDeposito = new System.Windows.Forms.TextBox();
            this.txtTaxaTaker = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTaxaMaker = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtJsonTickerPath = new System.Windows.Forms.TextBox();
            this.txtTickerResource = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtJsonOrderbookPath = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOrderbookResource = new System.Windows.Forms.TextBox();
            this.txtGatewayUrl = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboNome = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTagPrice = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTagAmount = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTagVolumn = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTagLast = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTagAsk = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTagBid = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtIntervalo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sigla";
            // 
            // txtSigla
            // 
            this.txtSigla.Location = new System.Drawing.Point(192, 32);
            this.txtSigla.Name = "txtSigla";
            this.txtSigla.Size = new System.Drawing.Size(100, 20);
            this.txtSigla.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtTaxaSaque);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtTaxaDeposito);
            this.groupBox1.Controls.Add(this.txtTaxaTaker);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtTaxaMaker);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(317, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(301, 119);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Taxas";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Taxa Saque";
            // 
            // txtTaxaSaque
            // 
            this.txtTaxaSaque.Location = new System.Drawing.Point(9, 77);
            this.txtTaxaSaque.Name = "txtTaxaSaque";
            this.txtTaxaSaque.Size = new System.Drawing.Size(100, 20);
            this.txtTaxaSaque.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(131, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Taxa Taker";
            // 
            // txtTaxaDeposito
            // 
            this.txtTaxaDeposito.Location = new System.Drawing.Point(134, 77);
            this.txtTaxaDeposito.Name = "txtTaxaDeposito";
            this.txtTaxaDeposito.Size = new System.Drawing.Size(100, 20);
            this.txtTaxaDeposito.TabIndex = 11;
            // 
            // txtTaxaTaker
            // 
            this.txtTaxaTaker.Location = new System.Drawing.Point(134, 32);
            this.txtTaxaTaker.Name = "txtTaxaTaker";
            this.txtTaxaTaker.Size = new System.Drawing.Size(100, 20);
            this.txtTaxaTaker.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(131, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Taxa Deposito";
            // 
            // txtTaxaMaker
            // 
            this.txtTaxaMaker.Location = new System.Drawing.Point(9, 32);
            this.txtTaxaMaker.Name = "txtTaxaMaker";
            this.txtTaxaMaker.Size = new System.Drawing.Size(100, 20);
            this.txtTaxaMaker.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Taxa Maker";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Ticker Resource";
            // 
            // txtJsonTickerPath
            // 
            this.txtJsonTickerPath.Location = new System.Drawing.Point(9, 212);
            this.txtJsonTickerPath.Name = "txtJsonTickerPath";
            this.txtJsonTickerPath.Size = new System.Drawing.Size(290, 20);
            this.txtJsonTickerPath.TabIndex = 23;
            // 
            // txtTickerResource
            // 
            this.txtTickerResource.Location = new System.Drawing.Point(9, 167);
            this.txtTickerResource.Name = "txtTickerResource";
            this.txtTickerResource.Size = new System.Drawing.Size(290, 20);
            this.txtTickerResource.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(220, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Caminho até Json do Ticker (separado por \';\')";
            // 
            // txtJsonOrderbookPath
            // 
            this.txtJsonOrderbookPath.Location = new System.Drawing.Point(9, 122);
            this.txtJsonOrderbookPath.Name = "txtJsonOrderbookPath";
            this.txtJsonOrderbookPath.Size = new System.Drawing.Size(290, 20);
            this.txtJsonOrderbookPath.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 106);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(240, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Caminho até Json do Orderbook (separado por \';\')";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Gateway Url";
            // 
            // txtOrderbookResource
            // 
            this.txtOrderbookResource.Location = new System.Drawing.Point(9, 77);
            this.txtOrderbookResource.Name = "txtOrderbookResource";
            this.txtOrderbookResource.Size = new System.Drawing.Size(290, 20);
            this.txtOrderbookResource.TabIndex = 17;
            // 
            // txtGatewayUrl
            // 
            this.txtGatewayUrl.Location = new System.Drawing.Point(9, 32);
            this.txtGatewayUrl.Name = "txtGatewayUrl";
            this.txtGatewayUrl.Size = new System.Drawing.Size(290, 20);
            this.txtGatewayUrl.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Orderbook Resource";
            // 
            // cboNome
            // 
            this.cboNome.FormattingEnabled = true;
            this.cboNome.Location = new System.Drawing.Point(9, 32);
            this.cboNome.Name = "cboNome";
            this.cboNome.Size = new System.Drawing.Size(177, 21);
            this.cboNome.TabIndex = 14;
            this.cboNome.SelectedValueChanged += new System.EventHandler(this.cboNome_SelectedValueChanged);
            this.cboNome.TextChanged += new System.EventHandler(this.cboNome_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtTagPrice);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtTagAmount);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtTagVolumn);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtTagLast);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtTagAsk);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtTagBid);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtJsonOrderbookPath);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtOrderbookResource);
            this.groupBox2.Controls.Add(this.txtJsonTickerPath);
            this.groupBox2.Controls.Add(this.txtTickerResource);
            this.groupBox2.Controls.Add(this.txtGatewayUrl);
            this.groupBox2.Location = new System.Drawing.Point(12, 141);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(606, 294);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Json";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 242);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(114, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Nome da Tag do Price";
            // 
            // txtTagPrice
            // 
            this.txtTagPrice.Location = new System.Drawing.Point(9, 258);
            this.txtTagPrice.Name = "txtTagPrice";
            this.txtTagPrice.Size = new System.Drawing.Size(290, 20);
            this.txtTagPrice.TabIndex = 35;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(302, 196);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(126, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Nome da Tag do Amount";
            // 
            // txtTagAmount
            // 
            this.txtTagAmount.Location = new System.Drawing.Point(305, 212);
            this.txtTagAmount.Name = "txtTagAmount";
            this.txtTagAmount.Size = new System.Drawing.Size(290, 20);
            this.txtTagAmount.TabIndex = 33;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(302, 151);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(125, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Nome da Tag do Volumn";
            // 
            // txtTagVolumn
            // 
            this.txtTagVolumn.Location = new System.Drawing.Point(305, 167);
            this.txtTagVolumn.Name = "txtTagVolumn";
            this.txtTagVolumn.Size = new System.Drawing.Size(290, 20);
            this.txtTagVolumn.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(302, 106);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(110, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Nome da Tag do Last";
            // 
            // txtTagLast
            // 
            this.txtTagLast.Location = new System.Drawing.Point(305, 122);
            this.txtTagLast.Name = "txtTagLast";
            this.txtTagLast.Size = new System.Drawing.Size(290, 20);
            this.txtTagLast.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(302, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Nome da Tag do Ask";
            // 
            // txtTagAsk
            // 
            this.txtTagAsk.Location = new System.Drawing.Point(305, 77);
            this.txtTagAsk.Name = "txtTagAsk";
            this.txtTagAsk.Size = new System.Drawing.Size(290, 20);
            this.txtTagAsk.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(302, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Nome da Tag do Bid";
            // 
            // txtTagBid
            // 
            this.txtTagBid.Location = new System.Drawing.Point(305, 33);
            this.txtTagBid.Name = "txtTagBid";
            this.txtTagBid.Size = new System.Drawing.Size(290, 20);
            this.txtTagBid.TabIndex = 25;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtIntervalo);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.cboNome);
            this.groupBox3.Controls.Add(this.txtSigla);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(12, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(299, 119);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            // 
            // txtIntervalo
            // 
            this.txtIntervalo.Location = new System.Drawing.Point(9, 77);
            this.txtIntervalo.Name = "txtIntervalo";
            this.txtIntervalo.Size = new System.Drawing.Size(177, 20);
            this.txtIntervalo.TabIndex = 16;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 61);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(128, 13);
            this.label18.TabIndex = 15;
            this.label18.Text = "Intervalo Atualização (ms)";
            // 
            // btnSalvar
            // 
            this.btnSalvar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSalvar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnSalvar.ForeColor = System.Drawing.Color.White;
            this.btnSalvar.Location = new System.Drawing.Point(443, 443);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(175, 34);
            this.btnSalvar.TabIndex = 16;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            // 
            // FrmExchange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(628, 489);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmExchange";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exchanges";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSigla;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTaxaSaque;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTaxaDeposito;
        private System.Windows.Forms.TextBox txtTaxaTaker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTaxaMaker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboNome;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtJsonTickerPath;
        private System.Windows.Forms.TextBox txtTickerResource;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtJsonOrderbookPath;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtOrderbookResource;
        private System.Windows.Forms.TextBox txtGatewayUrl;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTagPrice;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtTagAmount;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTagVolumn;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTagLast;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTagAsk;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTagBid;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtIntervalo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnSalvar;
    }
}