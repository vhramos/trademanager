﻿namespace TradeManager.Presentation.Forms
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPercentGanho = new System.Windows.Forms.Button();
            this.btnTotalTrades = new System.Windows.Forms.Button();
            this.btnTotalExchanges = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvExchanges = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgvParesExchanges = new System.Windows.Forms.DataGridView();
            this.clExecutar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.lblLucro = new System.Windows.Forms.Label();
            this.lblPrecoVenda = new System.Windows.Forms.Label();
            this.lblPrecoCompra = new System.Windows.Forms.Label();
            this.lblTotalTaxas = new System.Windows.Forms.Label();
            this.lblComprarEm = new System.Windows.Forms.Label();
            this.lblQuantidade = new System.Windows.Forms.Label();
            this.lblVenderEm = new System.Windows.Forms.Label();
            this.btnRealizarTrade = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chtVolume = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvTrades = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExchanges = new System.Windows.Forms.ToolStripMenuItem();
            this.marketDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aPIPrivadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wconnectManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkPermitirOrdensMaker = new System.Windows.Forms.CheckBox();
            this.chkTaxaDeposito = new System.Windows.Forms.CheckBox();
            this.chkTaxaSaque = new System.Windows.Forms.CheckBox();
            this.tmExchanges = new System.Windows.Forms.Timer(this.components);
            this.bgwExchanges = new System.ComponentModel.BackgroundWorker();
            this.gbMelhorNegocio = new System.Windows.Forms.GroupBox();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgvPrecoReal = new System.Windows.Forms.DataGridView();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExchanges)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParesExchanges)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chtVolume)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrades)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbMelhorNegocio.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrecoReal)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(655, 96);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 30;
            this.label3.Text = "% de Lucro";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(369, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Trades Realizadas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(173, 96);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "Exchanges";
            // 
            // btnPercentGanho
            // 
            this.btnPercentGanho.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnPercentGanho.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnPercentGanho.FlatAppearance.BorderSize = 0;
            this.btnPercentGanho.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnPercentGanho.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnPercentGanho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPercentGanho.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPercentGanho.ForeColor = System.Drawing.Color.White;
            this.btnPercentGanho.Location = new System.Drawing.Point(503, 33);
            this.btnPercentGanho.Margin = new System.Windows.Forms.Padding(2);
            this.btnPercentGanho.Name = "btnPercentGanho";
            this.btnPercentGanho.Size = new System.Drawing.Size(242, 92);
            this.btnPercentGanho.TabIndex = 24;
            this.btnPercentGanho.Text = "0";
            this.btnPercentGanho.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnPercentGanho.UseVisualStyleBackColor = false;
            // 
            // btnTotalTrades
            // 
            this.btnTotalTrades.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnTotalTrades.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnTotalTrades.FlatAppearance.BorderSize = 0;
            this.btnTotalTrades.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnTotalTrades.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnTotalTrades.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotalTrades.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotalTrades.ForeColor = System.Drawing.Color.White;
            this.btnTotalTrades.Location = new System.Drawing.Point(257, 33);
            this.btnTotalTrades.Margin = new System.Windows.Forms.Padding(2);
            this.btnTotalTrades.Name = "btnTotalTrades";
            this.btnTotalTrades.Size = new System.Drawing.Size(242, 92);
            this.btnTotalTrades.TabIndex = 23;
            this.btnTotalTrades.Text = "0";
            this.btnTotalTrades.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnTotalTrades.UseVisualStyleBackColor = false;
            // 
            // btnTotalExchanges
            // 
            this.btnTotalExchanges.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.btnTotalExchanges.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTotalExchanges.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.btnTotalExchanges.FlatAppearance.BorderSize = 0;
            this.btnTotalExchanges.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.btnTotalExchanges.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.btnTotalExchanges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotalExchanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotalExchanges.ForeColor = System.Drawing.Color.White;
            this.btnTotalExchanges.Location = new System.Drawing.Point(11, 33);
            this.btnTotalExchanges.Margin = new System.Windows.Forms.Padding(2);
            this.btnTotalExchanges.Name = "btnTotalExchanges";
            this.btnTotalExchanges.Size = new System.Drawing.Size(242, 92);
            this.btnTotalExchanges.TabIndex = 22;
            this.btnTotalExchanges.Text = "0";
            this.btnTotalExchanges.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnTotalExchanges.UseVisualStyleBackColor = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvExchanges);
            this.groupBox2.Location = new System.Drawing.Point(377, 130);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(839, 262);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Exchanges";
            // 
            // dgvExchanges
            // 
            this.dgvExchanges.AllowUserToAddRows = false;
            this.dgvExchanges.AllowUserToDeleteRows = false;
            this.dgvExchanges.AllowUserToOrderColumns = true;
            this.dgvExchanges.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvExchanges.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvExchanges.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvExchanges.BackgroundColor = System.Drawing.Color.White;
            this.dgvExchanges.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvExchanges.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvExchanges.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvExchanges.Location = new System.Drawing.Point(3, 19);
            this.dgvExchanges.Name = "dgvExchanges";
            this.dgvExchanges.ReadOnly = true;
            this.dgvExchanges.RowHeadersVisible = false;
            this.dgvExchanges.Size = new System.Drawing.Size(830, 236);
            this.dgvExchanges.TabIndex = 0;
            this.dgvExchanges.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExchanges_CellContentClick);
            this.dgvExchanges.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvExchanges_ColumnHeaderMouseClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgvParesExchanges);
            this.groupBox4.Location = new System.Drawing.Point(11, 130);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(360, 690);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            // 
            // dgvParesExchanges
            // 
            this.dgvParesExchanges.AllowUserToAddRows = false;
            this.dgvParesExchanges.AllowUserToDeleteRows = false;
            this.dgvParesExchanges.AllowUserToOrderColumns = true;
            this.dgvParesExchanges.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvParesExchanges.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvParesExchanges.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvParesExchanges.BackgroundColor = System.Drawing.Color.White;
            this.dgvParesExchanges.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvParesExchanges.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParesExchanges.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clExecutar});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvParesExchanges.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvParesExchanges.Location = new System.Drawing.Point(6, 12);
            this.dgvParesExchanges.Name = "dgvParesExchanges";
            this.dgvParesExchanges.ReadOnly = true;
            this.dgvParesExchanges.RowHeadersVisible = false;
            this.dgvParesExchanges.Size = new System.Drawing.Size(350, 671);
            this.dgvParesExchanges.TabIndex = 1;
            this.dgvParesExchanges.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvParesExchanges_CellContentClick);
            this.dgvParesExchanges.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvParesExchanges_ColumnHeaderMouseClick);
            // 
            // clExecutar
            // 
            this.clExecutar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.clExecutar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clExecutar.HeaderText = "";
            this.clExecutar.Name = "clExecutar";
            this.clExecutar.ReadOnly = true;
            this.clExecutar.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clExecutar.Text = ">>";
            this.clExecutar.UseColumnTextForButtonValue = true;
            this.clExecutar.Width = 30;
            // 
            // lblLucro
            // 
            this.lblLucro.AutoSize = true;
            this.lblLucro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLucro.Location = new System.Drawing.Point(6, 152);
            this.lblLucro.Name = "lblLucro";
            this.lblLucro.Size = new System.Drawing.Size(91, 16);
            this.lblLucro.TabIndex = 30;
            this.lblLucro.Text = "Lucro Líquido:";
            // 
            // lblPrecoVenda
            // 
            this.lblPrecoVenda.AutoSize = true;
            this.lblPrecoVenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecoVenda.Location = new System.Drawing.Point(6, 132);
            this.lblPrecoVenda.Name = "lblPrecoVenda";
            this.lblPrecoVenda.Size = new System.Drawing.Size(90, 16);
            this.lblPrecoVenda.TabIndex = 29;
            this.lblPrecoVenda.Text = "Preço Venda:";
            // 
            // lblPrecoCompra
            // 
            this.lblPrecoCompra.AutoSize = true;
            this.lblPrecoCompra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecoCompra.Location = new System.Drawing.Point(6, 112);
            this.lblPrecoCompra.Name = "lblPrecoCompra";
            this.lblPrecoCompra.Size = new System.Drawing.Size(98, 16);
            this.lblPrecoCompra.TabIndex = 28;
            this.lblPrecoCompra.Text = "Preço Compra:";
            // 
            // lblTotalTaxas
            // 
            this.lblTotalTaxas.AutoSize = true;
            this.lblTotalTaxas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalTaxas.Location = new System.Drawing.Point(6, 90);
            this.lblTotalTaxas.Name = "lblTotalTaxas";
            this.lblTotalTaxas.Size = new System.Drawing.Size(83, 16);
            this.lblTotalTaxas.TabIndex = 27;
            this.lblTotalTaxas.Text = "Total Taxas:";
            // 
            // lblComprarEm
            // 
            this.lblComprarEm.AutoSize = true;
            this.lblComprarEm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComprarEm.Location = new System.Drawing.Point(6, 30);
            this.lblComprarEm.Name = "lblComprarEm";
            this.lblComprarEm.Size = new System.Drawing.Size(85, 16);
            this.lblComprarEm.TabIndex = 0;
            this.lblComprarEm.Text = "Comprar em:";
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.AutoSize = true;
            this.lblQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.Location = new System.Drawing.Point(6, 70);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(81, 16);
            this.lblQuantidade.TabIndex = 2;
            this.lblQuantidade.Text = "Quantidade:";
            // 
            // lblVenderEm
            // 
            this.lblVenderEm.AutoSize = true;
            this.lblVenderEm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVenderEm.Location = new System.Drawing.Point(6, 50);
            this.lblVenderEm.Name = "lblVenderEm";
            this.lblVenderEm.Size = new System.Drawing.Size(77, 16);
            this.lblVenderEm.TabIndex = 1;
            this.lblVenderEm.Text = "Vender em:";
            // 
            // btnRealizarTrade
            // 
            this.btnRealizarTrade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRealizarTrade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnRealizarTrade.ForeColor = System.Drawing.Color.White;
            this.btnRealizarTrade.Location = new System.Drawing.Point(260, 222);
            this.btnRealizarTrade.Name = "btnRealizarTrade";
            this.btnRealizarTrade.Size = new System.Drawing.Size(175, 34);
            this.btnRealizarTrade.TabIndex = 4;
            this.btnRealizarTrade.Text = "Executar";
            this.btnRealizarTrade.UseVisualStyleBackColor = false;
            this.btnRealizarTrade.Click += new System.EventHandler(this.btnRealizarTrade_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.White;
            this.groupBox6.Controls.Add(this.chtVolume);
            this.groupBox6.Location = new System.Drawing.Point(1222, 33);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(441, 262);
            this.groupBox6.TabIndex = 34;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Volume";
            // 
            // chtVolume
            // 
            chartArea1.Name = "ChartArea1";
            this.chtVolume.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chtVolume.Legends.Add(legend1);
            this.chtVolume.Location = new System.Drawing.Point(6, 15);
            this.chtVolume.Name = "chtVolume";
            this.chtVolume.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.IsValueShownAsLabel = true;
            series1.LabelForeColor = System.Drawing.Color.White;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chtVolume.Series.Add(series1);
            this.chtVolume.Size = new System.Drawing.Size(429, 242);
            this.chtVolume.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgvTrades);
            this.groupBox3.Location = new System.Drawing.Point(380, 398);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(839, 36);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Histórico";
            // 
            // dgvTrades
            // 
            this.dgvTrades.AllowUserToAddRows = false;
            this.dgvTrades.AllowUserToDeleteRows = false;
            this.dgvTrades.AllowUserToOrderColumns = true;
            this.dgvTrades.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvTrades.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvTrades.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTrades.BackgroundColor = System.Drawing.Color.White;
            this.dgvTrades.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTrades.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvTrades.Location = new System.Drawing.Point(6, 19);
            this.dgvTrades.Name = "dgvTrades";
            this.dgvTrades.ReadOnly = true;
            this.dgvTrades.RowHeadersVisible = false;
            this.dgvTrades.Size = new System.Drawing.Size(827, 403);
            this.dgvTrades.TabIndex = 38;
            this.dgvTrades.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvTrades_ColumnHeaderMouseClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1370, 24);
            this.menuStrip1.TabIndex = 36;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraçõesToolStripMenuItem,
            this.menuExchanges});
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.editarToolStripMenuItem.Text = "Editar";
            // 
            // configuraçõesToolStripMenuItem
            // 
            this.configuraçõesToolStripMenuItem.Name = "configuraçõesToolStripMenuItem";
            this.configuraçõesToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.configuraçõesToolStripMenuItem.Text = "Configurações";
            this.configuraçõesToolStripMenuItem.Click += new System.EventHandler(this.configuraçõesToolStripMenuItem_Click);
            // 
            // menuExchanges
            // 
            this.menuExchanges.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.marketDataToolStripMenuItem,
            this.aPIPrivadaToolStripMenuItem,
            this.wconnectManagerToolStripMenuItem});
            this.menuExchanges.Name = "menuExchanges";
            this.menuExchanges.Size = new System.Drawing.Size(151, 22);
            this.menuExchanges.Text = "Exchanges";
            // 
            // marketDataToolStripMenuItem
            // 
            this.marketDataToolStripMenuItem.Name = "marketDataToolStripMenuItem";
            this.marketDataToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.marketDataToolStripMenuItem.Text = "Market Data";
            this.marketDataToolStripMenuItem.Click += new System.EventHandler(this.marketDataToolStripMenuItem_Click);
            // 
            // aPIPrivadaToolStripMenuItem
            // 
            this.aPIPrivadaToolStripMenuItem.Name = "aPIPrivadaToolStripMenuItem";
            this.aPIPrivadaToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.aPIPrivadaToolStripMenuItem.Text = "API Privada";
            this.aPIPrivadaToolStripMenuItem.Click += new System.EventHandler(this.aPIPrivadaToolStripMenuItem_Click);
            // 
            // wconnectManagerToolStripMenuItem
            // 
            this.wconnectManagerToolStripMenuItem.Name = "wconnectManagerToolStripMenuItem";
            this.wconnectManagerToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.wconnectManagerToolStripMenuItem.Text = "Wconnect Manager";
            this.wconnectManagerToolStripMenuItem.Click += new System.EventHandler(this.wconnectManagerToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkPermitirOrdensMaker);
            this.groupBox1.Controls.Add(this.chkTaxaDeposito);
            this.groupBox1.Controls.Add(this.chkTaxaSaque);
            this.groupBox1.Location = new System.Drawing.Point(750, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(466, 54);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            // 
            // chkPermitirOrdensMaker
            // 
            this.chkPermitirOrdensMaker.AutoSize = true;
            this.chkPermitirOrdensMaker.Location = new System.Drawing.Point(200, 31);
            this.chkPermitirOrdensMaker.Name = "chkPermitirOrdensMaker";
            this.chkPermitirOrdensMaker.Size = new System.Drawing.Size(130, 17);
            this.chkPermitirOrdensMaker.TabIndex = 24;
            this.chkPermitirOrdensMaker.Text = "Permitir Ordens Maker";
            this.chkPermitirOrdensMaker.UseVisualStyleBackColor = true;
            this.chkPermitirOrdensMaker.CheckedChanged += new System.EventHandler(this.ConfiguracaoTaxas_CheckedChanged);
            // 
            // chkTaxaDeposito
            // 
            this.chkTaxaDeposito.AutoSize = true;
            this.chkTaxaDeposito.Checked = true;
            this.chkTaxaDeposito.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTaxaDeposito.Location = new System.Drawing.Point(9, 31);
            this.chkTaxaDeposito.Name = "chkTaxaDeposito";
            this.chkTaxaDeposito.Size = new System.Drawing.Size(95, 17);
            this.chkTaxaDeposito.TabIndex = 22;
            this.chkTaxaDeposito.Text = "Taxa Depósito";
            this.chkTaxaDeposito.UseVisualStyleBackColor = true;
            this.chkTaxaDeposito.CheckedChanged += new System.EventHandler(this.ConfiguracaoTaxas_CheckedChanged);
            // 
            // chkTaxaSaque
            // 
            this.chkTaxaSaque.AutoSize = true;
            this.chkTaxaSaque.Checked = true;
            this.chkTaxaSaque.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTaxaSaque.Location = new System.Drawing.Point(110, 31);
            this.chkTaxaSaque.Name = "chkTaxaSaque";
            this.chkTaxaSaque.Size = new System.Drawing.Size(84, 17);
            this.chkTaxaSaque.TabIndex = 23;
            this.chkTaxaSaque.Text = "Taxa Saque";
            this.chkTaxaSaque.UseVisualStyleBackColor = true;
            this.chkTaxaSaque.CheckedChanged += new System.EventHandler(this.ConfiguracaoTaxas_CheckedChanged);
            // 
            // tmExchanges
            // 
            this.tmExchanges.Interval = 700;
            this.tmExchanges.Tick += new System.EventHandler(this.tmExchanges_Tick);
            // 
            // bgwExchanges
            // 
            this.bgwExchanges.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwExchanges_DoWork);
            // 
            // gbMelhorNegocio
            // 
            this.gbMelhorNegocio.BackColor = System.Drawing.Color.White;
            this.gbMelhorNegocio.Controls.Add(this.lblLucro);
            this.gbMelhorNegocio.Controls.Add(this.btnRealizarTrade);
            this.gbMelhorNegocio.Controls.Add(this.lblPrecoVenda);
            this.gbMelhorNegocio.Controls.Add(this.lblComprarEm);
            this.gbMelhorNegocio.Controls.Add(this.lblPrecoCompra);
            this.gbMelhorNegocio.Controls.Add(this.lblVenderEm);
            this.gbMelhorNegocio.Controls.Add(this.lblTotalTaxas);
            this.gbMelhorNegocio.Controls.Add(this.lblQuantidade);
            this.gbMelhorNegocio.Location = new System.Drawing.Point(545, 451);
            this.gbMelhorNegocio.Name = "gbMelhorNegocio";
            this.gbMelhorNegocio.Size = new System.Drawing.Size(441, 262);
            this.gbMelhorNegocio.TabIndex = 35;
            this.gbMelhorNegocio.TabStop = false;
            this.gbMelhorNegocio.Text = "Melhor Negócio";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.DividerWidth = 30;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::TradeManager.Presentation.Properties.Resources.SendIcon;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn1.Width = 197;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.panel3.BackgroundImage = global::TradeManager.Presentation.Properties.Resources.dashboard;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(503, 39);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(72, 56);
            this.panel3.TabIndex = 27;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.panel2.BackgroundImage = global::TradeManager.Presentation.Properties.Resources.exchange;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Location = new System.Drawing.Point(257, 39);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(72, 56);
            this.panel2.TabIndex = 28;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.panel1.BackgroundImage = global::TradeManager.Presentation.Properties.Resources.exchange;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(11, 39);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(72, 56);
            this.panel1.TabIndex = 26;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgvPrecoReal);
            this.groupBox5.Location = new System.Drawing.Point(1222, 558);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(441, 262);
            this.groupBox5.TabIndex = 38;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Preço Real";
            // 
            // dgvPrecoReal
            // 
            this.dgvPrecoReal.AllowUserToAddRows = false;
            this.dgvPrecoReal.AllowUserToDeleteRows = false;
            this.dgvPrecoReal.AllowUserToOrderColumns = true;
            this.dgvPrecoReal.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPrecoReal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvPrecoReal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPrecoReal.BackgroundColor = System.Drawing.Color.White;
            this.dgvPrecoReal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvPrecoReal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPrecoReal.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvPrecoReal.Location = new System.Drawing.Point(3, 19);
            this.dgvPrecoReal.Name = "dgvPrecoReal";
            this.dgvPrecoReal.ReadOnly = true;
            this.dgvPrecoReal.RowHeadersVisible = false;
            this.dgvPrecoReal.Size = new System.Drawing.Size(432, 236);
            this.dgvPrecoReal.TabIndex = 0;
            this.dgvPrecoReal.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPrecoReal_ColumnHeaderMouseClick);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.gbMelhorNegocio);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPercentGanho);
            this.Controls.Add(this.btnTotalTrades);
            this.Controls.Add(this.btnTotalExchanges);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TradeManager";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPrincipal_FormClosed);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExchanges)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvParesExchanges)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chtVolume)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrades)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbMelhorNegocio.ResumeLayout(false);
            this.gbMelhorNegocio.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrecoReal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPercentGanho;
        private System.Windows.Forms.Button btnTotalTrades;
        private System.Windows.Forms.Button btnTotalExchanges;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvExchanges;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgvParesExchanges;
        private System.Windows.Forms.Label lblLucro;
        private System.Windows.Forms.Label lblPrecoVenda;
        private System.Windows.Forms.Label lblPrecoCompra;
        private System.Windows.Forms.Label lblTotalTaxas;
        private System.Windows.Forms.Label lblComprarEm;
        private System.Windows.Forms.Label lblQuantidade;
        private System.Windows.Forms.Label lblVenderEm;
        private System.Windows.Forms.Button btnRealizarTrade;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkTaxaDeposito;
        private System.Windows.Forms.CheckBox chkTaxaSaque;
        private System.Windows.Forms.Timer tmExchanges;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtVolume;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuExchanges;
        private System.ComponentModel.BackgroundWorker bgwExchanges;
        private System.Windows.Forms.DataGridView dgvTrades;
        private System.Windows.Forms.GroupBox gbMelhorNegocio;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewButtonColumn clExecutar;
        private System.Windows.Forms.CheckBox chkPermitirOrdensMaker;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgvPrecoReal;
        private System.Windows.Forms.ToolStripMenuItem marketDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aPIPrivadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wconnectManagerToolStripMenuItem;
    }
}