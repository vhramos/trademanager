﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradeManager.Application.Interface.AppService;
using TradeManager.CrossCutting.Extensions;
using TradeManager.Domain.Entities;
using TradeManager.Presentation.Util;

namespace TradeManager.Presentation.Forms
{
    public partial class FrmConfiguracoes : Form
    {
        private ParametrosAplicacao Parametros { get; set; }
        private readonly IParametroAplicacaoAppService _parametroAplicacaoAppService;
        public FrmConfiguracoes(IParametroAplicacaoAppService parametroAplicacaoAppService)
        {
            _parametroAplicacaoAppService = parametroAplicacaoAppService;
            Parametros = _parametroAplicacaoAppService.ConsultarParametros();
            InitializeComponent();
        }
        private void FrmConfiguracoes_Load(object sender, EventArgs e)
        {
            if (Parametros != null)
            {
                txtLucro.Text = Parametros.AlertaPercentLucro.ToString();
                txtVolume.Text = Parametros.AlertaVolume.ToString();
                chkAtivarSom.Checked = Parametros.EmitirSomAlerta;
            }
        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (Parametros == null)
                Parametros = new ParametrosAplicacao();
            Parametros.AlertaPercentLucro = txtLucro.Text.Converter<double>();
            Parametros.AlertaVolume = txtVolume.Text.Converter<double>();
            Parametros.EmitirSomAlerta = chkAtivarSom.Checked;
            _parametroAplicacaoAppService.AlterarParametros(Parametros);
            MessageBox.Show("Configurações alteradas com sucesso!", "Configurações", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }
}
