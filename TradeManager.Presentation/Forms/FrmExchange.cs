﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradeManager.Application.Interface.AppService;
using TradeManager.CrossCutting.Extensions;
using TradeManager.Domain.Entities;

namespace TradeManager.Presentation.Forms
{
    public partial class FrmExchange : Form
    {
        private Exchange _exchangeSelecionada;
        public Exchange ExchangeSelecionada
        {
            get
            {
                if (_exchangeSelecionada == null)
                    _exchangeSelecionada = MontarExchange();
                return _exchangeSelecionada;
            }
            set
            {
                _exchangeSelecionada = value;
            }
        }
        private readonly IExchangeAppService _exchangeAppService;
        public FrmExchange(IExchangeAppService exchangeAppService)
        {
            _exchangeAppService = exchangeAppService;
            InitializeComponent();
            cboNome.DataSource = _exchangeAppService.GetAll().Select(e => e.NomeExchange).ToList();
        }
        private void CarregarTela()
        {
            txtSigla.Text = ExchangeSelecionada.SiglaExchange;
            txtIntervalo.Text = ExchangeSelecionada.IntervaloAtualizacao.ToString();
            txtTaxaDeposito.Text = ExchangeSelecionada.TaxaDeposito.ToString();
            txtTaxaMaker.Text = ExchangeSelecionada.TaxaMaker.ToString();
            txtTaxaSaque.Text = ExchangeSelecionada.TaxaSaque.ToString();
            txtTaxaTaker.Text = ExchangeSelecionada.TaxaTaker.ToString();

            txtGatewayUrl.Text = ExchangeSelecionada.GatewayUrl;
            txtJsonOrderbookPath.Text = ExchangeSelecionada.OrderBookJsonPath;
            txtJsonTickerPath.Text = ExchangeSelecionada.TickerJsonPath;
            txtOrderbookResource.Text = ExchangeSelecionada.OrderBookResourcePath;
            txtTagAmount.Text = ExchangeSelecionada.JsonOrderBookAmountPath;
            txtTagAsk.Text = ExchangeSelecionada.JsonAskName;
            txtTagBid.Text = ExchangeSelecionada.JsonBidName;
            txtTagLast.Text = ExchangeSelecionada.JsonLastName;
            txtTagPrice.Text = ExchangeSelecionada.JsonOrderBookPricePath;
            txtTagVolumn.Text = ExchangeSelecionada.JsonVolumnName;
            txtTickerResource.Text = ExchangeSelecionada.TickerResourcePath;
        }
        private Exchange MontarExchange()
        {
            return new Exchange()
            {
                SiglaExchange = txtSigla.Text,
                IntervaloAtualizacao = txtIntervalo.Text.Converter<int>(),
                TaxaDeposito = txtTaxaDeposito.Text.Converter<double>(),
                TaxaMaker = txtTaxaMaker.Text.Converter<double>(),
                TaxaSaque = txtTaxaSaque.Text.Converter<double>(),
                TaxaTaker = txtTaxaTaker.Text.Converter<double>(),
                GatewayUrl = txtGatewayUrl.Text,
                OrderBookJsonPath = txtJsonOrderbookPath.Text,
                TickerJsonPath = txtJsonTickerPath.Text,
                OrderBookResourcePath = txtOrderbookResource.Text,
                JsonOrderBookAmountPath = txtTagAmount.Text,
                JsonAskName = txtTagAsk.Text,
                JsonBidName = txtTagBid.Text,
                JsonLastName = txtTagLast.Text,
                JsonOrderBookPricePath = txtTagPrice.Text,
                JsonVolumnName = txtTagVolumn.Text,
                TickerResourcePath = txtTickerResource.Text,
            };
        }
        private void LimparTela()
        {
            txtSigla.Clear();
            txtIntervalo.Clear();
            txtTaxaDeposito.Clear();
            txtTaxaMaker.Clear();
            txtTaxaSaque.Clear();
            txtTaxaTaker.Clear();
            txtGatewayUrl.Clear();
            txtJsonOrderbookPath.Clear();
            txtJsonTickerPath.Clear();
            txtOrderbookResource.Clear();
            txtTagAmount.Clear();
            txtTagAsk.Clear();
            txtTagBid.Clear();
            txtTagLast.Clear();
            txtTagPrice.Clear();
            txtTagVolumn.Clear();
            txtTickerResource.Clear();
        }
        private void cboNome_TextChanged(object sender, EventArgs e)
        {
            if (!cboNome.Items.Contains(cboNome.Text))
                LimparTela();
        }
        private void cboNome_SelectedValueChanged(object sender, EventArgs e)
        {
            ExchangeSelecionada = _exchangeAppService.ConsultarPorNome(cboNome.Text);
            if (ExchangeSelecionada != null)
                CarregarTela();
        }
    }
}
