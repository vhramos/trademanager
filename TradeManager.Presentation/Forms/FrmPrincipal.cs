﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Deployment.Application;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradeManager.Application.AppService;
using TradeManager.Application.Interface.AppService;
using TradeManager.Domain.Entities;
using TradeManager.Presentation.ViewModel;
using System.Reflection;
using ExchangeBot.ViewModel;
using TradeManager.Application.Interface.Task;
using Newtonsoft.Json;
using NAudio.Wave;
using System.Threading;
using WConnectBase;
using QuantumTrader;
using QuantumTrader.BitFinex;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Data.Entity;
using TradeManager.CrossCutting.Extensions;
using System.Security.Principal;
using System.Configuration;
using TradeManager.Presentation.Util;
using WConnect;

namespace TradeManager.Presentation.Forms
{
    public partial class FrmPrincipal : Form
    {
        private readonly object lockobj = new object();
        private readonly IExchangeAppService _exchangeAppService;
        private readonly ICotacaoMoedaAppService _cotacaoMoedaAppService;
        private readonly ITradeAppService _tradeAppService;
        private readonly IExchangeTask _exchangeTask;
        private readonly ICotacaoMoedaTask _cotacaoMoedaTask;
        private readonly IParametroAplicacaoAppService _parametroAplicacaoAppService;
        private Trade MelhorTrade;
        private string TextoMelhorTrade
        {
            get
            {
                return string.Concat(lblComprarEm.Text, Environment.NewLine,
                                     lblVenderEm.Text, Environment.NewLine,
                                     lblQuantidade.Text, Environment.NewLine,
                                     lblTotalTaxas.Text, Environment.NewLine,
                                     lblPrecoCompra.Text, Environment.NewLine,
                                     lblPrecoVenda.Text, Environment.NewLine,
                                     lblLucro.Text, Environment.NewLine,
                                     "Deseja Executar?");
            }
        }
        private WConnectForm WConnectForm { get; set; }
        private SortData GridParExchangesSort;
        private SortData GridExchangesOrderSort;
        private SortData GridTradesOrderSort;
        private SortData GridPrecoRealOrderSort;
        public FrmPrincipal(IExchangeAppService exchangeAppService,
                            ICotacaoMoedaAppService cotacaoMoedaAppService,
                            ITradeAppService tradeAppService,
                            IExchangeTask exchangeTask,
                            ICotacaoMoedaTask cotacaoMoedaTask,
                            IParametroAplicacaoAppService parametroAplicacaoAppService)
        {
            _cotacaoMoedaTask = cotacaoMoedaTask;
            _exchangeTask = exchangeTask;
            _tradeAppService = tradeAppService;
            _exchangeAppService = exchangeAppService;
            _cotacaoMoedaAppService = cotacaoMoedaAppService;
            _parametroAplicacaoAppService = parametroAplicacaoAppService;
            InitializeComponent();
            ConfigurarOpcoesDeTaxas();
            tmExchanges.Start();
            LogarTrades();
            GridParExchangesSort = new SortData();
            GridExchangesOrderSort = new SortData();
            GridTradesOrderSort = new SortData();
            GridPrecoRealOrderSort = new SortData();
            btnTotalExchanges.Text = _exchangeAppService.GetAll().ToList().Count.ToString();
        }
        private void ExibirWConnect()
        {
            var moduloOrdens = _tradeAppService.ConsultarModuloOrdens();
            if (WConnectForm == null || WConnectForm.IsDisposed)
            {
                WConnectForm = new WConnectForm(moduloOrdens, moduloOrdens.GetExchanges().ToList(), _tradeAppService.ConsultarParesMoedas());
                WConnectForm.Show();
            }
        }
        private void MontarAmbienteInicial()
        {
            var exchanges = _exchangeAppService.ConsultarExchangesCarregadas();
            if (exchanges.Count > 0)
            {
                CarregarGridExchanges(exchanges);
                CarregarGridValorReal(exchanges);
                MontarGraficoVolume(_exchangeAppService.ConsultarExchangesCarregadasOnline());
                CarregarGridParExchanges();
            }
        }
        private void CarregarGridValorReal(List<Exchange> exchanges)
        {
            if (IsHandleCreated)
            {
                List<PrecoRealExchangeViewModel> lstPrecoReal = new List<PrecoRealExchangeViewModel>();
                foreach (var exchange in exchanges)
                {
                    PrecoRealExchangeViewModel precoRealVM = new PrecoRealExchangeViewModel(exchange);
                    lstPrecoReal.Add(precoRealVM);
                }
                dgvPrecoReal.Invoke(new EventHandler(delegate
                {
                    dgvPrecoReal.DataSource = lstPrecoReal.OrderByIndex(GridPrecoRealOrderSort);
                }));
            }
        }
        private void CarregarGridExchanges(List<Exchange> exchanges)
        {
            if (IsHandleCreated)
            {
                List<ExchangeViewModel> lstGridExchanges = new List<ExchangeViewModel>();
                foreach (var exchange in exchanges)
                {
                    ExchangeViewModel exchangeRow = new ExchangeViewModel(exchange, _exchangeAppService.CalcularValorAgio(exchange));
                    lstGridExchanges.Add(exchangeRow);
                }
                dgvExchanges.Invoke(new EventHandler(delegate
                {
                    dgvExchanges.DataSource = lstGridExchanges.OrderByIndex(GridExchangesOrderSort).ToList();
                }));
            }
        }
        private void CarregarGridParExchanges()
        {
            if (IsHandleCreated)
            {
                lock (lockobj)
                {
                    var trades = _tradeAppService.ConsultarPossiveisTrades().ToList();
                    List<PossiveisTradesViewModel> lstPares = new List<PossiveisTradesViewModel>();
                    foreach (var trade in trades)
                    {
                        PossiveisTradesViewModel pairRow = new PossiveisTradesViewModel(trade);
                        lstPares.Add(pairRow);
                    }

                    dgvParesExchanges.Invoke(new EventHandler(delegate
                    {
                        bool possuiPares = lstPares.Count > 0;
                        gbMelhorNegocio.Visible = possuiPares;
                        if (possuiPares)
                        {
                            lstPares = lstPares.OrderByIndex(GridParExchangesSort).ToList();
                            dgvParesExchanges.DataSource = lstPares;
                            MelhorTrade = lstPares.First(t => t.Trade.LucroLiquido == lstPares.Max(l => l.Trade.LucroLiquido)).Trade;
                            dgvParesExchanges.Rows[lstPares.FindIndex(t => t.Trade == MelhorTrade)].DefaultCellStyle.BackColor = Color.FromArgb(255, 93, 184, 92);
                            MontarMelhorTrade();
                            if (_tradeAppService.AlertarMelhorTrade(MelhorTrade))
                            {
                                DialogResult diagExecutarTrade = MessageBox.Show(TextoMelhorTrade, "Alerta Trade", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                                if (diagExecutarTrade == DialogResult.Yes)
                                    ExecutarTrade(MelhorTrade);
                            }
                        }
                        else
                            dgvParesExchanges.DataSource = null;
                    }));
                }
            }
        }
        private void MontarGraficoVolume(List<Exchange> exchanges)
        {
            if (IsHandleCreated)
            {
                chtVolume.Invoke(new EventHandler(delegate
                {
                    chtVolume.Series.First().Points.Clear();
                    foreach (var exchange in exchanges)
                    {
                        chtVolume.Series.First().Points.AddXY(exchange.SiglaExchange, Math.Round(exchange.Ticker.Volumn, 1));
                    }
                }));
            }
        }
        private void MontarMelhorTrade()
        {
            lblComprarEm.Text = string.Concat("Comprar em: ", MelhorTrade.ExchangeAsk.SiglaExchange);
            lblVenderEm.Text = string.Concat("Vender em: ", MelhorTrade.ExchangeBid.SiglaExchange);
            lblQuantidade.Text = string.Concat("Quantidade: ", MelhorTrade.QuantidadeCompraBTC.ToBitcoinFormat());
            lblTotalTaxas.Text = string.Concat("Total Taxas: ", MelhorTrade.TotalTaxas.ToPercentFormat());
            lblPrecoCompra.Text = string.Concat("Preço Compra: ", MelhorTrade.ExchangeAsk.PrecoAsk.ToRealFormat());
            lblPrecoVenda.Text = string.Concat("Preço Venda: ", MelhorTrade.ExchangeBid.PrecoBid.ToRealFormat());
            lblLucro.Text = string.Concat("Lucro Líquido: ", MelhorTrade.LucroLiquido.ToRealFormat());
            btnPercentGanho.Text = MelhorTrade.PercentualLucroLiquido.ToPercentFormat();
        }
        private void tmExchanges_Tick(object sender, EventArgs e)
        {
            if (!bgwExchanges.IsBusy)
                MontarAmbienteInicial();
        }
        private void btnRealizarTrade_Click(object sender, EventArgs e)
        {
            ExecutarTrade(MelhorTrade);
        }
        private void bgwExchanges_DoWork(object sender, DoWorkEventArgs e)
        {
            MontarAmbienteInicial();
        }
        private void LogarTrades()
        {
            var moduloOrdens = _tradeAppService.ConsultarModuloOrdens();
            List<HistoricoOrdensViewModel> lstOrdens = new List<HistoricoOrdensViewModel>();
            for (int i = 0; i < moduloOrdens.GetNumOrders(); i++)
                lstOrdens.Add(new HistoricoOrdensViewModel(moduloOrdens.GetOrder(i)));
            btnTotalTrades.Text = lstOrdens.Count.ToString();
            dgvTrades.DataSource = lstOrdens.OrderByIndex(GridTradesOrderSort);
        }
        private void configuraçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConfiguracoes config = new FrmConfiguracoes(_parametroAplicacaoAppService);
            config.ShowDialog();
        }
        private void dgvExchanges_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex > -1)
            {
                Exchange exchangeSel = ((ExchangeViewModel)dgvExchanges.Rows[e.RowIndex].DataBoundItem).Exchange;
                exchangeSel.ExchangeOnline = !exchangeSel.ExchangeOnline;
                _exchangeAppService.Update(exchangeSel);
            }
        }
        private void dgvParesExchanges_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
                ExecutarTrade(((PossiveisTradesViewModel)dgvParesExchanges.Rows[e.RowIndex].DataBoundItem).Trade);
        }
        private void ExecutarTrade(Trade trade)
        {
            ExibirWConnect();
            _tradeAppService.ExecutarTrade(trade);
            LogarTrades();
        }
        private void ConfiguracaoTaxas_CheckedChanged(object sender, EventArgs e)
        {
            ConfigurarOpcoesDeTaxas();
        }
        private void ConfigurarOpcoesDeTaxas()
        {
            _exchangeAppService.ConfigurarOpcoesDeTaxas(chkTaxaDeposito.Checked, chkTaxaSaque.Checked, chkPermitirOrdensMaker.Checked);
        }
        private void ConfigurarOrdenacaoDataGrid(DataGridView dataGridView, int propertyIndex, int columnIndex, SortData sortData)
        {
            sortData.PropertyIndex = propertyIndex;
            sortData.Ascending = !sortData.Ascending;
            SortOrder order = SortOrder.Ascending;
            if (!sortData.Ascending)
                order = SortOrder.Descending;
            dataGridView.Columns[columnIndex].HeaderCell.SortGlyphDirection = order;
        }
        private void dgvParesExchanges_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex > 0)
                ConfigurarOrdenacaoDataGrid(dgvParesExchanges, e.ColumnIndex - 1, e.ColumnIndex, GridParExchangesSort);
        }
        private void dgvExchanges_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ConfigurarOrdenacaoDataGrid(dgvExchanges, e.ColumnIndex, e.ColumnIndex, GridExchangesOrderSort);
        }
        private void dgvTrades_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ConfigurarOrdenacaoDataGrid(dgvTrades, e.ColumnIndex, e.ColumnIndex, GridTradesOrderSort);
            LogarTrades();
        }
        private void dgvPrecoReal_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ConfigurarOrdenacaoDataGrid(dgvPrecoReal, e.ColumnIndex, e.ColumnIndex, GridPrecoRealOrderSort);
        }
        private void FrmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }
        private void marketDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmExchange exchanges = new FrmExchange(_exchangeAppService);
            exchanges.ShowDialog();
        }
        private void aPIPrivadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Configuracoes().Show();
        }
        private void wconnectManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExibirWConnect();
        }
    }
}