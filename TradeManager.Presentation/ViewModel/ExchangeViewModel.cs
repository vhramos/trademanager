﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.CrossCutting.Extensions;
using TradeManager.Domain.Entities;
using WConnectBase;

namespace ExchangeBot.ViewModel
{
    public class ExchangeViewModel
    {
        [Browsable(false)]
        public Exchange Exchange { get; set; }
        public bool Habiltada { get; set; }
        public double Ping { get; set; }
        public string NomeExchange { get; set; }
        public string BidAmount { get; set; }
        public string Bid { get; set; }
        public string Ask { get; set; }
        public string AskAmount { get; set; }
        public string Last { get; set; }
        public string Agio { get; set; }
        public string SaldoBTC { get; set; }
        public string SaldoBRL { get; set; }
        public string Volumn { get; set; }

        public ExchangeViewModel(Exchange exchange, double agio)
        {
            Exchange = exchange;
            Habiltada = exchange.ExchangeOnline;
            NomeExchange = exchange.SiglaExchange;
            Ping = exchange.Ping;
            Bid = exchange.PrecoBid.ToRealFormat();
            BidAmount = exchange.QuantidadeBid.ToBitcoinFormat();
            Ask = exchange.PrecoAsk.ToRealFormat();
            AskAmount = exchange.QuantidadeAsk.ToBitcoinFormat();
            Last = exchange.Ticker.Last.ToRealFormat();
            Volumn = exchange.Ticker.Volumn.ToBitcoinFormat();
            Agio = agio.ToPercentFormat();
            SaldoBTC = exchange.SaldoBTC.ToBitcoinFormat();
            SaldoBRL = exchange.SaldoBRL.ToRealFormat();
        }
    }
}

