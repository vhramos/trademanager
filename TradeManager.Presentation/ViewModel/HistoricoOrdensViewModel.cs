﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.CrossCutting.Extensions;
using WConnectBase;

namespace TradeManager.Presentation.ViewModel
{
    public class HistoricoOrdensViewModel
    {
        [Browsable(false)]
        public ExComOrder Order { get; set; }
        public string Exchange
        {
            get
            {
                return Order.Exchange;
            }
        }
        public string Tipo
        {
            get
            {
                return Order.Type.ToString();
            }
        }
        public string Acao
        {
            get
            {
                return Order.Action.ToString();
            }
        }
        public string Timestamp
        {
            get
            {
                return Order.Timestamp.ToString();
            }
        }
        public string Preco
        {
            get
            {
                return Order.BasePrice.ToRealFormat();
            }
        }
        public string Quantidade
        {
            get
            {
                return Order.Amount.ToBitcoinFormat();
            }
        }
        public string Status
        {
            get
            {
                return Order.Status.ToString();
            }
        }
        public HistoricoOrdensViewModel(ExComOrder order)
        {
            Order = order;
        }
    }
}
