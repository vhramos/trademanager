﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.CrossCutting.Extensions;
using TradeManager.Domain.Entities;

namespace TradeManager.Presentation.ViewModel
{
    public class PrecoRealExchangeViewModel
    {
        [Browsable(false)]
        public Exchange Exchange { get; set; }
        public string NomeExchange
        {
            get
            {
                return Exchange.SiglaExchange;
            }
        }
        public string PrecoRealBid
        {
            get
            {
                return Exchange.PrecoRealBid.ToRealFormat();
            }
        }
        public string PrecoRealAsk
        {
            get
            {
                return Exchange.PrecoRealAsk.ToRealFormat();
            }
        }
        public PrecoRealExchangeViewModel(Exchange exchange)
        {
            Exchange = exchange;
        }
    }
}
