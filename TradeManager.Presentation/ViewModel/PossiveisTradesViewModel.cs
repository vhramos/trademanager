﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradeManager.Application.Interface.AppService;
using TradeManager.CrossCutting.Extensions;
using TradeManager.Domain.Entities;

namespace TradeManager.Presentation.ViewModel
{
    public class PossiveisTradesViewModel
    {
        [Browsable(false)]
        public Trade Trade { get; set; }
        public string NomePar
        {
            get
            {
                return string.Concat(Trade.ExchangeAsk.SiglaExchange, "/", Trade.ExchangeBid.SiglaExchange);
            }
        }
        public string PercentLucroLiquido
        {
            get
            {
                return Trade.PercentualLucroLiquido.ToPercentFormat();
            }
        }
        public string LucroLiquido
        {
            get
            {
                return Trade.LucroLiquido.ToRealFormat();
            }
        }
        public PossiveisTradesViewModel(Trade trade)
        {
            Trade = trade;
        }
    }
}
