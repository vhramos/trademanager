﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Domain.Entities
{
    public class CotacaoMoeda
    {
        public double Valor { get; set; }
        public DateTime DataCotacaoUTC { get; set; }
    }
}
