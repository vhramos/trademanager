﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Domain.Entities
{
    public class Exchange
    {
        public int ExchangeId { get; set; }
        public string SiglaExchange { get; set; }
        public string NomeExchange { get; set; }
        [NotMapped]
        public OrderBook OrderBook { private get; set; }
        [NotMapped]
        public Ticker Ticker { get; set; }
        [NotMapped]
        public long Ping { get; set; }
        [NotMapped]
        public bool AplicarTaxaSaque { get; set; }
        [NotMapped]
        public bool AplicarTaxaDeposito { get; set; }
        [NotMapped]
        public bool ExecutarOrdensMaker { get; set; }
        [NotMapped]
        public double PrecoAsk
        {
            get
            {
                double preco = OrderBook.MelhorAsk.Price;
                if (ExecutarOrdensMaker)
                    preco -= 0.01;
                return preco;
            }
        }
        [NotMapped]
        public double PrecoBid
        {
            get
            {
                double preco = OrderBook.MelhorBid.Price;
                if (ExecutarOrdensMaker)
                    preco += 0.01;
                return preco;
            }
        }
        [NotMapped]
        public double PrecoRealAsk
        {
            get
            {
                return PrecoAsk * (1 + (TaxasAsk / 100));
            }
        }
        [NotMapped]
        public double PrecoRealBid
        {
            get
            {
                return PrecoBid * (1 - (TaxasBid / 100));
            }
        }
        [NotMapped]
        public double TaxasAsk
        {
            get
            {
                double taxas = 0;
                if (ExecutarOrdensMaker)
                    taxas += TaxaMaker;

                if (AplicarTaxaDeposito)
                    taxas += TaxaDeposito;

                return taxas;
            }
        }
        [NotMapped]
        public double TaxasBid
        {
            get
            {
                double taxas = 0;
                if (ExecutarOrdensMaker)
                    taxas += TaxaMaker;

                if (AplicarTaxaSaque)
                    taxas += TaxaSaque;
                return taxas;
            }
        }
        [NotMapped]
        public double QuantidadeAsk
        {
            get
            {
                return OrderBook.MelhorAsk.Amount;
            }
        }
        [NotMapped]
        public double QuantidadeBid
        {
            get
            {
                return OrderBook.MelhorBid.Amount;
            }
        }
        [NotMapped]
        public bool DadosCarregados
        {
            get
            {
                return OrderBook != null && Ticker != null;
            }
        }
        [NotMapped]
        public double SaldoBRL { get; set; }
        [NotMapped]
        public double SaldoBTC { get; set; }
        public bool ExchangeOnline { get; set; }
        public double TaxaMaker { get; set; }
        public double TaxaTaker { get; set; }
        public double TaxaDeposito { get; set; }
        public double TaxaSaque { get; set; }
        public string GatewayUrl { get; set; }
        public string OrderBookResourcePath { get; set; }
        public string OrderBookJsonPath { get; set; }
        public string TickerResourcePath { get; set; }
        public string TickerJsonPath { get; set; }
        public string JsonBidName { get; set; }
        public string JsonAskName { get; set; }
        public string JsonLastName { get; set; }
        public string JsonVolumnName { get; set; }
        public string JsonOrderBookAmountPath { get; set; }
        public string JsonOrderBookPricePath { get; set; }
        public int IntervaloAtualizacao { get; set; }
    }
}
