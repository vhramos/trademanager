﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Domain.Entities
{
    public class Ticker
    {
        public double Volumn { get; set; }
        public double Last { get; set; }
    }
}
