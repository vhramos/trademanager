﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Domain.Entities
{
    public class Usuario
    {
        public int UsuarioID { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public bool PrimeiroAcesso { get; set; } = true;
    }
}
