﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Domain.Entities
{
    public class Order
    {
        public double Amount { get; set; }
        public double Price { get; set; }
    }
}
