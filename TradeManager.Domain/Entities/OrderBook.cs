﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Domain.Entities
{
    /// <summary>
    /// Entidade OrderBook
    /// </summary>
    public class OrderBook
    {
        /// <summary>
        /// Asks (vendas) do orderbook
        /// </summary>
        public List<Order> Asks { get; set; }
        /// <summary>
        /// Bids (compras) do orderbook
        /// </summary>
        public List<Order> Bids { get; set; }
        /// <summary>
        /// Maior preço de compra do orderbook
        /// </summary>
        public Order MelhorBid
        {
            get
            {
                Order bid = new Order();
                if (Bids.Count > 0)
                    bid = Bids.First(b => b.Price == Bids.Max(p => p.Price));
                return bid;
            }
        }
        /// <summary>
        /// Menor preço de venda do orderbook
        /// </summary>
        public Order MelhorAsk
        {
            get
            {
                Order ask = new Order();
                if (Asks.Count > 0)
                    ask = Asks.First(a => a.Price == Asks.Min(p => p.Price));
                return ask;
            }
        }
        /// <summary>
        /// Construtor
        /// </summary>
        public OrderBook()
        {
            Asks = new List<Order>();
            Bids = new List<Order>();
        }
    }
}
