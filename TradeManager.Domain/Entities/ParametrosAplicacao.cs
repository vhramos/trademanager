﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Domain.Entities
{
    public class ParametrosAplicacao
    {
        public int ParametrosAplicacaoId { get; set; }
        public double AlertaVolume { get; set; }
        public double AlertaPercentLucro { get; set; }
        public bool EmitirSomAlerta { get; set; }
    }
}
