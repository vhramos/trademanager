﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.Domain.Entities
{
    public class Trade
    {
        [Key]
        public int TradeId { get; set; }
        public Exchange ExchangeAsk { get; }
        public Exchange ExchangeBid { get; }
        public double QuantidadeCompraBTC
        {
            get
            {
                double comprar = 0;
                if (ExchangeAsk.QuantidadeAsk >= ExchangeBid.QuantidadeBid)
                    comprar = ExchangeBid.QuantidadeBid;
                else
                    comprar = ExchangeAsk.QuantidadeAsk;

                if (comprar * ExchangeAsk.PrecoAsk > ExchangeAsk.SaldoBRL)
                    comprar = ExchangeAsk.SaldoBRL / ExchangeAsk.PrecoAsk;

                return comprar;
            }
        }
        public double LucroLiquido
        {
            get
            {

                return (ExchangeBid.PrecoRealBid - ExchangeAsk.PrecoRealAsk) * QuantidadeCompraBTC;
            }
        }
        public double PercentualLucroLiquido
        {
            get
            {
                return (((ExchangeBid.PrecoBid / ExchangeAsk.PrecoAsk) - 1) * 100) - TotalTaxas;
            }
        }
        public double TotalTaxas
        {
            get
            {
                return ExchangeAsk.TaxasAsk + ExchangeBid.TaxasBid;
            }
        }
        public Trade()
        {
        }
        public Trade(Exchange exchangeA, Exchange exchangeB)
        {
            if (exchangeA.PrecoBid > exchangeB.PrecoBid)
            {
                ExchangeBid = exchangeA;
                ExchangeAsk = exchangeB;
            }
            else
            {
                ExchangeBid = exchangeB;
                ExchangeAsk = exchangeA;
            }
        }
    }
}
