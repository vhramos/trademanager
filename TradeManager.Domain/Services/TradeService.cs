﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Repositories;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Domain.Services
{
    public class TradeService : ServiceBase<Trade>, ITradeService
    {
        public TradeService(ITradeRepository tradeRepository)
            : base(tradeRepository)
        {
        }

    }
}
