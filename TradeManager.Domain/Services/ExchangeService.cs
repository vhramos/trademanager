﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Repositories;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Domain.Services
{
    public class ExchangeService : ServiceBase<Exchange>, IExchangeService
    {
        public ExchangeService(IExchangeRepository exchangeRepository)
            : base(exchangeRepository)
        {
        }
    }
}
