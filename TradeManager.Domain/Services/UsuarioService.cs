﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Repositories;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Domain.Services
{
    public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
    {
        public UsuarioService(IUsuarioRepository usuarioRepository)
            : base(usuarioRepository)
        {
        }
    }
}
