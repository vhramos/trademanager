﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Singleton;
using TradeManager.CrossCutting.Crypto;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Services;
using WConnectBase;

namespace TradeManager.Application.AppService
{
    public class UsuarioAppService : AppServiceBase<Usuario>, IUsuarioAppService
    {
        private readonly IUsuarioService _usuarioService;
        public UsuarioAppService(IUsuarioService usuarioService) : base(usuarioService)
        {
            _usuarioService = usuarioService;
        }
        public Usuario EfetuarLogin(Usuario usuario)
        {
            return _usuarioService.GetAll().FirstOrDefault(u => u.Login == usuario.Login && u.Senha == CryptoUtil.HashPassword(usuario.Senha));
        }
    }
}
