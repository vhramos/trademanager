﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Singleton;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Application.AppService
{
    public class ParametrosAplicacaoAppService : AppServiceBase<ParametrosAplicacao>, IParametroAplicacaoAppService
    {
        private readonly IParametrosAplicacaoService _parametrosAplicacaoService;
        public ParametrosAplicacaoAppService(IParametrosAplicacaoService parametrosAplicacaoService) : base(parametrosAplicacaoService)
        {
            _parametrosAplicacaoService = parametrosAplicacaoService;
        }
        public void AlterarParametros(ParametrosAplicacao parametros)
        {
            if (ConsultarParametros() == null)
                _parametrosAplicacaoService.Add(parametros);
            else
                _parametrosAplicacaoService.Update(parametros);
            TradeManagerSingleton.ParametrosAplicacao = parametros;
        }
        public ParametrosAplicacao ConsultarParametros()
        {
            if (TradeManagerSingleton.ParametrosAplicacao == null)
                TradeManagerSingleton.ParametrosAplicacao = _parametrosAplicacaoService.GetAll().FirstOrDefault();
            return TradeManagerSingleton.ParametrosAplicacao;
        }
    }
}
