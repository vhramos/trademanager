﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Singleton;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Services;
using WConnectBase;

namespace TradeManager.Application.AppService
{
    public class TradeAppService : AppServiceBase<Trade>, ITradeAppService
    {
        private readonly IExchangeAppService _exchangeAppService;
        private readonly IParametroAplicacaoAppService _parametroAplicacaoAppService;
        public TradeAppService(ITradeService tradeService,
                               IExchangeAppService exchangeAppService,
                               IParametroAplicacaoAppService parametroAplicacaoAppService)
            : base(tradeService)
        {
            _exchangeAppService = exchangeAppService;
            _parametroAplicacaoAppService = parametroAplicacaoAppService;
        }
        public bool AlertarMelhorTrade(Trade trade)
        {
            ParametrosAplicacao parametros = _parametroAplicacaoAppService.ConsultarParametros();
            bool alertar = parametros != null &&
                           (TradeManagerSingleton.UltimaTradeAlertada == null ||
                           trade.PercentualLucroLiquido != TradeManagerSingleton.UltimaTradeAlertada.PercentualLucroLiquido ||
                           trade.QuantidadeCompraBTC != TradeManagerSingleton.UltimaTradeAlertada.QuantidadeCompraBTC) &&
                           trade.PercentualLucroLiquido >= parametros.AlertaPercentLucro &&
                           trade.QuantidadeCompraBTC >= parametros.AlertaVolume;
            if (alertar)
                TradeManagerSingleton.UltimaTradeAlertada = trade;
            return alertar;
        }
        public List<Pair> ConsultarParesMoedas()
        {
            List<Pair> pairs = new List<Pair>();
            pairs.Add(new Pair("BTC", "BRL"));
            return pairs;
        }
        public ExComManager ConsultarModuloOrdens()
        {
            return TradeManagerSingleton.ModuloOrdens;
        }
        public IEnumerable<Trade> ConsultarPossiveisTrades()
        {
            var exchangesTmp = _exchangeAppService.ConsultarExchangesCarregadasOnline();
            List<Trade> lstTrades = new List<Trade>();
            foreach (var exchange1 in _exchangeAppService.ConsultarExchangesCarregadasOnline())
            {
                foreach (var exchange2 in exchangesTmp)
                {
                    if (exchange1.ExchangeId != exchange2.ExchangeId)
                    {
                        Trade trade = new Trade(exchange1, exchange2);
                        lstTrades.Add(trade);
                    }
                }
                exchangesTmp.RemoveAll(e => e.ExchangeId == exchange1.ExchangeId);
            }
            return lstTrades.OrderByDescending(t => t.LucroLiquido);
        }
        public async void ExecutarTrade(Trade trade)
        {
            await Task.Run(() =>
            {
                var moduloOrdens = ConsultarModuloOrdens();
                if (moduloOrdens.GetExchanges().Any(e => e.Name == trade.ExchangeAsk.NomeExchange) && moduloOrdens.GetExchanges().Any(e => e.Name == trade.ExchangeBid.NomeExchange))
                {
                    moduloOrdens.PlaceOrder(trade.ExchangeAsk.NomeExchange, ConsultarParesMoedas().First(), ExComOrder.OrderAction.Buy, ExComOrder.OrderType.Limit, trade.ExchangeAsk.PrecoAsk, trade.QuantidadeCompraBTC, "Compra - Arbitragem Manual");
                    moduloOrdens.PlaceOrder(trade.ExchangeBid.NomeExchange, ConsultarParesMoedas().First(), ExComOrder.OrderAction.Sell, ExComOrder.OrderType.Limit, trade.ExchangeBid.PrecoBid, trade.QuantidadeCompraBTC, "Venda - Arbitragem Manual");
                }
            });
        }
    }
}
