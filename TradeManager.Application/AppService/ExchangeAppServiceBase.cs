﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TradeManager.Application.Interface;
using TradeManager.Application.Interface.AppService;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Application.AppService
{
    public class ExchangeAppServiceBase<TEntity> : IExchangeAppServiceBase<TEntity> where TEntity : class
    {
        private readonly IExchangeService _exchangeService;
        public OrderBook OrderBook { get; set; }
        public Ticker Ticker { get; set; }
        public ExchangeAppServiceBase(IExchangeService exchangeService)
        {
            _exchangeService = exchangeService;
            OrderBook = new OrderBook();
            Ticker = new Ticker();
        }
        public virtual void ConfigurarOrderBook(JToken orderBookJson)
        {
            throw new NotImplementedException();
        }
        public virtual void ConfigurarTicker(JToken tickerJson)
        {
            throw new NotImplementedException();
        }
        public double CalcularQuantidadeCompra(Exchange exchangeBid, Exchange exchangeAsk)
        {
            return _exchangeService.CalcularQuantidadeCompra(exchangeBid, exchangeAsk);
        }
        public double CalcularPercentualGanho(Exchange exchangeBid, Exchange exchangeAsk, bool aplicarTaxaDeposito, bool aplicarTaxaSaque)
        {
            return _exchangeService.CalcularPercentualGanho(exchangeBid, exchangeAsk, aplicarTaxaDeposito, aplicarTaxaSaque);
        }
    }
}
