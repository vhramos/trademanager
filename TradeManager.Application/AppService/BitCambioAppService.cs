﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TradeManager.Application.Interface;
using TradeManager.Application.Interface.AppService;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Application.AppService
{
    public class BitCambioAppService : ExchangeAppServiceBase<Exchange>, IBitCambioAppService
    {
        private readonly IExchangeService _exchangeService;

        public BitCambioAppService(IExchangeService exchangeService) : base(exchangeService)
        {
            _exchangeService = exchangeService;
        }
        public override void ConfigurarOrderBook(JToken orderBookJson)
        {
            if (orderBookJson != null)
            {
                foreach (var bid in orderBookJson["bids"])
                    OrderBook.Bids.Add(new Order() { Price = Convert.ToDouble(bid[0]), Amount = Convert.ToDouble(bid[1]) });
                foreach (var ask in orderBookJson["asks"])
                    OrderBook.Asks.Add(new Order() { Price = Convert.ToDouble(ask[0]), Amount = Convert.ToDouble(ask[1]) });
            }
        }

        public override void ConfigurarTicker(JToken tickerJson)
        {
            if (tickerJson != null)
            {
                Ticker.Last = Convert.ToDouble(tickerJson["last"]);
                Ticker.Volumn = Convert.ToDouble(tickerJson["vol"]);
            }
        }
    }
}
