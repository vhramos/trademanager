﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Singleton;
using TradeManager.CrossCutting.Extensions;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Application.AppService
{
    public class CotacaoMoedaAppService : AppServiceBase<CotacaoMoeda>, ICotacaoMoedaAppService
    {
        public CotacaoMoedaAppService() : base(null)
        {
            if (TradeManagerSingleton.CotacaoBitcoin == null)
                TradeManagerSingleton.CotacaoBitcoin = new CotacaoMoeda();

            if (TradeManagerSingleton.CotacaoDolar == null)
                TradeManagerSingleton.CotacaoDolar = new CotacaoMoeda();
        }
        public CotacaoMoeda ConsultarCotacaoBitcoin()
        {
            return TradeManagerSingleton.CotacaoBitcoin;
        }
        public CotacaoMoeda ConsultarCotacaoDolar()
        {
            return TradeManagerSingleton.CotacaoDolar;
        }
    }
}
