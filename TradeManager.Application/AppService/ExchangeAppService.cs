﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TradeManager.Application.Interface;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Interface.Task;
using TradeManager.Application.Singleton;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Services;

namespace TradeManager.Application.AppService
{
    public class ExchangeAppService : AppServiceBase<Exchange>, IExchangeAppService
    {
        private readonly IExchangeService _exchangeService;
        private readonly ICotacaoMoedaAppService _cotacaoMoedaAppService;
        public ExchangeAppService(IExchangeService exchangeService,
                                  ICotacaoMoedaAppService cotacaoMoedaAppService)
            : base(exchangeService)
        {
            _cotacaoMoedaAppService = cotacaoMoedaAppService;
            _exchangeService = exchangeService;
            if (TradeManagerSingleton.Exchanges == null)
                TradeManagerSingleton.Exchanges = GetAll().ToList();
        }
        public double CalcularValorAgio(Exchange exchange)
        {
            return Math.Round(((exchange.PrecoAsk / (_cotacaoMoedaAppService.ConsultarCotacaoBitcoin().Valor * _cotacaoMoedaAppService.ConsultarCotacaoDolar().Valor)) - 1) * 100, 2);
        }
        public void ConfigurarOpcoesDeTaxas(bool aplicarTaxaDeposito, bool aplicarTaxaSaque, bool executarOrdemMaker)
        {
            foreach (var exchange in TradeManagerSingleton.Exchanges)
            {
                exchange.AplicarTaxaDeposito = aplicarTaxaDeposito;
                exchange.AplicarTaxaSaque = aplicarTaxaSaque;
                exchange.ExecutarOrdensMaker = executarOrdemMaker;
            }
        }
        public List<Exchange> ConsultarExchangesCarregadas()
        {
            return TradeManagerSingleton.Exchanges.FindAll(e => e.DadosCarregados);
        }
        public List<Exchange> ConsultarExchangesCarregadasOnline()
        {
            return ConsultarExchangesCarregadas().FindAll(e => e.ExchangeOnline);
        }
        public Exchange ConsultarPorNome(string nome)
        {
            return _exchangeService.GetAll().FirstOrDefault(e => e.NomeExchange == nome);
        }
    }
}
