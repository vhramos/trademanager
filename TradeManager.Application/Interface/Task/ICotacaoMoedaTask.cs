﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;

namespace TradeManager.Application.Interface.Task
{
    public interface ICotacaoMoedaTask : ITaskBase<CotacaoMoeda>
    {
    }
}
