﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;
using WConnectBase;

namespace TradeManager.Application.Interface.AppService
{
    public interface IUsuarioAppService : IAppServiceBase<Usuario>
    {
        Usuario EfetuarLogin(Usuario usuario);
    }
}
