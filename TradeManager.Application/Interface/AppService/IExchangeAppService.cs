﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;

namespace TradeManager.Application.Interface.AppService
{
    public interface IExchangeAppService : IAppServiceBase<Exchange>
    {
        double CalcularValorAgio(Exchange exchange);
        List<Exchange> ConsultarExchangesCarregadas();
        List<Exchange> ConsultarExchangesCarregadasOnline();
        Exchange ConsultarPorNome(string nome);
        void ConfigurarOpcoesDeTaxas(bool aplicarTaxaDeposito, bool aplicarTaxaSaque, bool executarOrdemMaker);
    }
}
