﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;

namespace TradeManager.Application.Interface.AppService
{
    public static class Singleton
    {
        private static readonly object lockobj = new object();
        private static List<Exchange> _exchanges;

        public static List<Exchange> Exchanges
        {

            get
            {
                if (_exchanges == null)
                    _exchanges = new List<Exchange>();
                return _exchanges;
            }
            set
            {
                //lock (lockobj)
                //{
                _exchanges = value;
                //}
            }
        }
    }
}
