﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;

namespace TradeManager.Application.Interface.AppService
{
    public interface IParametroAplicacaoAppService : IAppServiceBase<ParametrosAplicacao>
    {
        void AlterarParametros(ParametrosAplicacao parametros);
        ParametrosAplicacao ConsultarParametros();
    }
}
