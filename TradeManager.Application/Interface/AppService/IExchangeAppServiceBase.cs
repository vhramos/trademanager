﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;

namespace TradeManager.Application.Interface.AppService
{
    public interface IExchangeAppServiceBase<TEntity> where TEntity : class
    {
        OrderBook OrderBook { get; set; }
        Ticker Ticker { get; set; }
        void ConfigurarOrderBook(JToken orderBookJson);
        void ConfigurarTicker(JToken tickerJson);
        double CalcularQuantidadeCompra(Exchange exchangeBid, Exchange exchangeAsk);
        double CalcularPercentualGanho(Exchange exchangeBid, Exchange exchangeAsk, bool aplicarTaxaDeposito, bool aplicarTaxaSaque);
    }
}
