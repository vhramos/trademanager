﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Domain.Entities;
using WConnectBase;

namespace TradeManager.Application.Interface.AppService
{
    public interface ITradeAppService : IAppServiceBase<Trade>
    {
        IEnumerable<Trade> ConsultarPossiveisTrades();
        bool AlertarMelhorTrade(Trade trade);
        void ExecutarTrade(Trade trade);
        ExComManager ConsultarModuloOrdens();
        List<Pair> ConsultarParesMoedas();
    }
}
