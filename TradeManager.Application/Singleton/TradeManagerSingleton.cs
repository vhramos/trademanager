﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Application.Interface.AppService;
using TradeManager.Domain.Entities;
using WConnectBase;
using QuantumTrader;
using QuantumTrader.BitcoinToYou;
using QuantumTrader.BitcoinTrade;
using QuantumTrader.BitFinex;
using QuantumTrader.BitStamp;
using QuantumTrader.BlinkTrade;
using QuantumTrader.FoxBit;
using QuantumTrader.Kraken;
using QuantumTrader.Braziliex;

namespace TradeManager.Application.Singleton
{
    public static class TradeManagerSingleton
    {
        #region Propriedades
        public static List<Exchange> Exchanges { get; set; }
        public static CotacaoMoeda CotacaoBitcoin { get; set; }
        public static CotacaoMoeda CotacaoDolar { get; set; }
        public static Trade UltimaTradeAlertada { get; set; }
        private static ExComManager _moduloOrdens;
        public static ExComManager ModuloOrdens
        {
            get
            {
                if (_moduloOrdens == null)
                    _moduloOrdens = MontarObjetoExcomManager();
                return _moduloOrdens;
            }
        }
        public static ParametrosAplicacao ParametrosAplicacao { get; set; }
        #endregion

        #region Metodos
        private static ExComManager MontarObjetoExcomManager()
        {
            ExComManager orderManager = new ExComManager();
            BlinkTradeClient blinkTrade = new BlinkTradeClient("BitCambio");
            blinkTrade.Connect();
            FoxBitClient foxBit = new FoxBitClient("FoxBit");
            foxBit.Connect();
            BitcoinTradeClient bitcoinTrade = new BitcoinTradeClient("BitcoinTrade");
            bitcoinTrade.Connect();
            BitcoinToYouClient bitcoinToYou = new BitcoinToYouClient("BitcoinToYou");
            bitcoinToYou.Connect();
            BraziliexClient braziliex = new BraziliexClient("Braziliex");
            braziliex.Connect();
            orderManager.AddExchange(blinkTrade);
            orderManager.AddExchange(foxBit);
            orderManager.AddExchange(bitcoinTrade);
            orderManager.AddExchange(bitcoinToYou);
            orderManager.AddExchange(braziliex);
            return orderManager;
        }
        #endregion
    }
}
