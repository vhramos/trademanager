﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Interface.Task;
using TradeManager.CrossCutting.Extensions;
using TradeManager.Domain.Entities;


namespace TradeManager.Application.Tasks
{
    public class TaskBitcambio : TaskBase<Exchange>, ITaskBitcambio
    {
        private readonly IBitCambioAppService _bitCambioAppService;

        public TaskBitcambio(IBitCambioAppService bitCambioAppService)
        {
            _bitCambioAppService = bitCambioAppService;
        }
        public override int Intervalo
        {
            get
            {
                return 500;
            }
        }

        public override async void Executar()
        {
            await Task.Run(() =>
            {
                var orderBookJson = HttpUtil.RetornarResponseJson("http://bitcambio_api.blinktrade.com/", "api/v1/BRL/orderbook");
                var tickerJson = HttpUtil.RetornarResponseJson("http://bitcambio_api.blinktrade.com/", "api/v1/BRL/ticker");
                _bitCambioAppService.ConfigurarOrderBook(orderBookJson);
                _bitCambioAppService.ConfigurarTicker(orderBookJson);
            });
        }

    }
}
