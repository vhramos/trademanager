﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Interface.Task;
using TradeManager.Application.Singleton;
using TradeManager.CrossCutting.Extensions;
using TradeManager.Domain.Entities;
using TradeManager.Domain.Interfaces.Services;
using WConnectBase;

namespace TradeManager.Application.Tasks
{
    public class ExchangeTask : TaskBase<Exchange>, IExchangeTask
    {
        public async override void Executar()
        {
            if (TradeManagerSingleton.Exchanges != null)
            {
                foreach (var exchange in TradeManagerSingleton.Exchanges)
                {
                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    await AtualizarOrderBook(exchange);
                    await AtualizarTicker(exchange);
                    await AtualizarSaldos(exchange);
                    exchange.Ping = watch.ElapsedMilliseconds;
                }
            }
        }
        private async Task AtualizarOrderBook(Exchange exchange)
        {
            await Task.Run(() =>
            {
                OrderBook orderBook = new OrderBook();
                var orderBookJson = HttpUtil.RetornarResponseJson(exchange.GatewayUrl, exchange.OrderBookResourcePath);
                if (orderBookJson != null)
                {
                    if (!string.IsNullOrEmpty(exchange.OrderBookJsonPath))
                    {
                        foreach (string tag in exchange.OrderBookJsonPath.Split(';'))
                        {
                            orderBookJson = orderBookJson[tag];
                        }
                    }
                    object priceKey = exchange.JsonOrderBookPricePath;
                    object amountKey = exchange.JsonOrderBookAmountPath;

                    if (int.TryParse(priceKey.ToString(), out int priceIndex))
                        priceKey = priceIndex;
                    if (int.TryParse(amountKey.ToString(), out int amountIndex))
                        amountKey = amountIndex;

                    foreach (var bid in orderBookJson[exchange.JsonBidName])
                    {
                        orderBook.Bids.Add(new Order()
                        {
                            Price = Convert.ToDouble(bid[priceKey]),
                            Amount = Convert.ToDouble(bid[amountKey])
                        });
                    }
                    foreach (var ask in orderBookJson[exchange.JsonAskName])
                    {
                        orderBook.Asks.Add(new Order()
                        {
                            Price = Convert.ToDouble(ask[priceKey]),
                            Amount = Convert.ToDouble(ask[amountKey])
                        });
                    }
                    exchange.OrderBook = orderBook;
                }
            });
        }
        private async Task AtualizarTicker(Exchange exchange)
        {
            await Task.Run(() =>
            {
                Ticker ticker = new Ticker();
                var tickerJson = HttpUtil.RetornarResponseJson(exchange.GatewayUrl, exchange.TickerResourcePath);
                if (tickerJson != null)
                {
                    if (!string.IsNullOrEmpty(exchange.TickerJsonPath))
                    {
                        foreach (string tag in exchange.TickerJsonPath.Split(';'))
                        {
                            tickerJson = tickerJson[tag];
                        }
                    }
                    object lastKey = exchange.JsonLastName;
                    object volumnKey = exchange.JsonVolumnName;
                    if (int.TryParse(lastKey.ToString(), out int lastIndex))
                        lastKey = lastIndex;
                    if (int.TryParse(volumnKey.ToString(), out int volumnIndex))
                        volumnKey = volumnIndex;
                    ticker.Last = Convert.ToDouble(tickerJson[lastKey]);
                    ticker.Volumn = Convert.ToDouble(tickerJson[volumnKey]);
                    exchange.Ticker = ticker;
                }
            });
        }
        private async Task AtualizarSaldos(Exchange exchange)
        {
            await Task.Run(() =>
            {
                EXComBase ordExchange = TradeManagerSingleton.ModuloOrdens.GetExchanges().FirstOrDefault(e => e.Name == exchange.NomeExchange);
                if (ordExchange != null)
                {
                    exchange.SaldoBRL = ordExchange.GetBalance("BRL");
                    exchange.SaldoBTC = ordExchange.GetBalance("BTC");
                }
                else
                {
                    exchange.SaldoBRL = 100000;
                    exchange.SaldoBTC = 100000;
                }
            });
        }
    }
}
