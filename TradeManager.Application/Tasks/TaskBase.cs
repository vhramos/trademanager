﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using TradeManager.Application.Interface.Task;

namespace TradeManager.Application.Tasks
{
    public class TaskBase<TEntity> : ITaskBase<TEntity> where TEntity : class
    {
        private readonly Timer timerTask = new Timer(500);
        public virtual int Intervalo
        {
            get
            {
                return 500;
            }
        }
        public TaskBase()
        {
            timerTask.AutoReset = true;
            timerTask.Elapsed += TmrExecutar_Elapsed;
            timerTask.Start();
        }
        private void TmrExecutar_Elapsed(object sender, ElapsedEventArgs e)
        {
            timerTask.Stop();
            Executar();
            timerTask.Start();
        }
        public virtual void Executar()
        {
            throw new NotImplementedException();
        }
    }
}
