﻿using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System;

namespace TradeManager.CrossCutting.Crypto
{
    public static class CryptoUtil
    {
        public static string HashPassword(string password)
        {
            var provider = new SHA1CryptoServiceProvider();
            var encoding = new UnicodeEncoding();
            return Convert.ToBase64String(provider.ComputeHash(encoding.GetBytes(password)));
        }
    }
}