﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeManager.CrossCutting.Extensions
{
    public static class HttpUtil
    {
        private const string JSON_CONTENT_TYPE = "application/json";
        public static JToken RetornarResponseJson(string url, string rota)
        {
            try
            {
                JToken json = null;
                var client = new RestClient(url);
                var request = new RestRequest(rota, Method.GET);
                request.OnBeforeDeserialization = resp => resp.ContentType = JSON_CONTENT_TYPE;
                var response = client.Execute(request);
                if (response.IsSuccessful)
                    json = JToken.Parse(response.Content);
                return json;
            }
            catch (Exception)
            {
                return default(JToken);
            }
        }
    }
}
