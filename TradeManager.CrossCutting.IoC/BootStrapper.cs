﻿using Ninject;
using TradeManager.Application.AppService;
using TradeManager.Application.Interface.AppService;
using TradeManager.Application.Interface.Task;
using TradeManager.Application.Tasks;
using TradeManager.Domain.Interfaces.Repositories;
using TradeManager.Domain.Interfaces.Services;
using TradeManager.Domain.Services;
using TradeManager.Infra.Data.Repositories;

namespace TradeManager.CrossCutting.IoC
{
    public static class BootStrapper
    {
        public static void RegisterServices(IKernel container)
        {

            #region AppService
            container.Bind<ICotacaoMoedaAppService>().To<CotacaoMoedaAppService>();
            container.Bind<IExchangeAppService>().To<ExchangeAppService>();
            container.Bind<ITradeAppService>().To<TradeAppService>();
            container.Bind<IUsuarioAppService>().To<UsuarioAppService>();
            container.Bind<IParametroAplicacaoAppService>().To<ParametrosAplicacaoAppService>().InTransientScope();
            #endregion

            #region Service
            container.Bind<IExchangeService>().To<ExchangeService>();
            container.Bind<ITradeService>().To<TradeService>();
            container.Bind<IUsuarioService>().To<UsuarioService>();
            container.Bind<IParametrosAplicacaoService>().To<ParametrosAplicacaoService>().InTransientScope();
            #endregion

            #region repository
            container.Bind<ITradeRepository>().To<TradeRepository>();
            container.Bind<IExchangeRepository>().To<ExchangeRepository>();
            container.Bind<IUsuarioRepository>().To<UsuarioRepository>();
            container.Bind<IParametrosAplicacaoRepository>().To<ParametrosAplicacaoRepository>().InTransientScope();
            #endregion

            #region task
            container.Bind<IExchangeTask>().To<ExchangeTask>().InSingletonScope();
            container.Bind<ICotacaoMoedaTask>().To<CotacaoMoedaTask>().InSingletonScope();
            #endregion
        }
    }
}
